height = 16;
width = 18;
depth = 8.4;

inner_height = 15.2;
inner_width = 12.2;
inner_depth = 4.7;

lip_height = 1.8;
lip_width = 16;
lip_depth = 6.4;

difference() {
    translate([ -width / 2, - depth / 2, 0 ]) cube([width, depth, height]);
    translate([ -inner_width / 2, - inner_depth / 2, 0 ]) cube([inner_width, inner_depth, inner_height]);
    translate([ -lip_width / 2, - lip_depth / 2, 0 ]) cube([lip_width, lip_depth, lip_height]);
}

