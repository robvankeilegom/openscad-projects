// General shape
top_dia = 19.6;
bottom_dia = 24.5;
height = 13.4;

// Donut shape
donut_rad = 3.75;
donut_inner_rad = 6;
donut_standoff = 2.1;

// Inner hole
through_hole_dia = 6.4;
recess_dia = 12.5;
recess_height = 3;

difference() {
    difference() {
        // General shape
        union() {
            translate([0, 0, height / 2])
                cylinder(height / 2, d = top_dia, center = false);
            cylinder(height / 2, d = bottom_dia, center = false);
        }

        //Donut
        rotate_extrude(convexity = 10, $fn = 100)
            translate([donut_inner_rad + donut_rad, donut_standoff + donut_rad, 0])
            circle(r = donut_rad, $fn = 100);
    }

    // Inner hole
    cylinder(height, d = through_hole_dia, center = false);
    translate([0, 0, height - recess_height])
        cylinder(recess_height, d = recess_dia, center = false);
}


